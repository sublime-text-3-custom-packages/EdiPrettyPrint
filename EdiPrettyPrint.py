import sublime
import sublime_plugin


class EdiPrettyPrintCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        # Get the entire region
        view_region = sublime.Region(0, self.view.size())
        # Check if ISA envelope is present
        isa00 = self.view.substr(sublime.Region(0, 3))
        seg_delim = self.view.substr(sublime.Region(105, 106))
        if isa00 == "ISA":
        	edi_with_newlines = self.view.substr(view_region).replace(seg_delim, seg_delim + "\n")
        	# Replace the view text with the output from toprettyxml() method
        	self.view.replace(edit, view_region, edi_with_newlines)
        else:
        	print("Not valid EDI.")